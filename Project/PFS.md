﻿# CS546 Project 09/25

Long list: 

OrangeFS

BeeGFS

1. HDFS [https://github.com/apache/hadoop](https://github.com/apache/hadoop)

2. ClusterTech [https://github.com/clustertech/cpfs-os](https://github.com/clustertech/cpfs-os)

3. PVFS [https://sumitn.github.io/pvfs/](https://sumitn.github.io/pvfs/)

4. Alluxio [https://github.com/Alluxio/alluxio](https://github.com/Alluxio/alluxio)

5. MooseFS [https://github.com/moosefs/moosefs](https://github.com/moosefs/moosefs)

6. xtreemFS [https://github.com/xtreemfs/xtreemfs](https://github.com/xtreemfs/xtreemfs)

7. quantcastFS [https://github.com/quantcast/qfs](https://github.com/quantcast/qfs)

8. LizardFS [https://github.com/lizardfs/lizardfs](https://github.com/lizardfs/lizardfs)

9. RozoFS [https://github.com/rozofs/rozofs](https://github.com/rozofs/rozofs)

10. Ceph [https://github.com/ceph/ceph](https://github.com/ceph/ceph)

11. OpenAFS [https://github.com/ceph/ceph](https://github.com/ceph/ceph) (Probably not parallel)

12. Isilon OneFS [https://github.com/ltp/EMC-Isilon-OneFS](https://github.com/ltp/EMC-Isilon-OneFS)

13. Not parallel DropletFS [https://github.com/scality/dropletfs](https://github.com/scality/dropletfs)

14. Tahoe_Lafs [https://github.com/tahoe-lafs/tahoe-lafs](https://github.com/tahoe-lafs/tahoe-lafs)

15. IPFS [https://github.com/ipfs/ipfs](https://github.com/ipfs/ipfs)

16. Hpe hfs [https://github.com/hpe-storage/csi-driver](https://github.com/hpe-storage/csi-driver)

17. Zfs [https://github.com/zfsonlinux/zfs](https://github.com/zfsonlinux/zfs)

18. Xfs [https://github.com/torvalds/linux/tree/master/fs/xfs](https://github.com/torvalds/linux/tree/master/fs/xfs)

19. MetaFS [https://github.com/pidydx/metafs](https://github.com/pidydx/metafs)

20. PLFS [https://github.com/plfs/plfs-core](https://github.com/plfs/plfs-core)

21. DeltaFS [https://github.com/pdlfs/deltafs](https://github.com/pdlfs/deltafs)

22. GIGA+ [https://github.com/achivetta/skyefs](https://github.com/achivetta/skyefs)

23. ADIOS 2 [https://github.com/ornladios/ADIOS2](https://github.com/ornladios/ADIOS2)

24. NOVA FS [https://github.com/NVSL/linux-nova](https://github.com/NVSL/linux-nova)

25. Linux CIFS [https://github.com/Distrotech/cifs-utils](https://github.com/Distrotech/cifs-utils)


# Features

|                |Serverless|Applications Own metadata|Journaled|Protocols|Data partition|Fault tolerance|Implementation|
|----------------|-------------------------------|-----------------------------|-------------------------------|-------------------------------|-------------------------------|-------------------------------|-------------------------------|
|HDFS|Server as Namenode|Some operations|Yes|HDFS protocol |64Mb|Yes|Large data, small size application|
|ClusterTech|Every node as a server|Server own metadata|Yes|IP IPoIB|64Mb|Yes|N/A AI, commercial?|
|PVFS|Client-Server structure|Server own metadata, App request for once|Yes|PVFS protocol|N/A|Yes|Linux kernel|
|Alluxio|Server|Yes|Yes|IP|Clustered|Yes|AI IoT|
|MooseFS|Cluster|Yes|Yes|IP Posix|N/A|Yes|Linux kernel|
|xtreemFS|Server and Cluster|Application own replications|NO (Asynchronized)|xtreamFS protocol|Yes when replicate data|Yes|Network area|
|quantcastFS|Meta server, chunk server, client|Yes|N/A probably NO|QFS protocol|Yes|Yes|MapReduce Data mining|
|LizardFS|Server|Yes when replication|No|IP|64Mb|Yes|Data mining, HPC|
|RozoFS |Cluster|Yes|No Super server keep synchronized|IP|Yes don't know the size|Yes replication|MapRecude Data mining|
|Ceph|Clusters (Storage nodes)|Yes|No|`cephx` protocol|N/A|Yes|General HPC|
|OpenAFS|Server Client|Yes|Yes|N/A probably IP|64Mb|Yes|Linux AFS|
|Isilon OneFS|Server|Yes|No|IP|N/A|Yes|Dell, HPC|
|DropletFS|N/A|N/A|N/A|N/A|N/A|N/A|N/A|
|Tahoe_Lafs|Storage Server and client|Yes|No|TCP/IP|No|Yes |General HPC |
|IPFS|Storage Server and client|Yes|No|TCP/IP|No|Yes |General|
|Hpe hfs|Clustered database and client|Yes|Yes|CXFS protocol|64Mb|Yes |HPC |
|Zfs|Volumn manager and File system, Storage pool|Yes|No|N/A|No|Yes |General|
|Xfs|Linux File storage|Yes|Yes|N/A|No|Yes |Linux kernel|
|MetaFS|Local File Storage|Yes|No|N/A|No|Yes |Linux kernel VM|
|PLFS|Local File Storage|Yes|No|N/A|No|Yes |Linux kernel VM|
|DeltaFS|Storage Server and client|Yes|No|TCP/IP|No|Yes |General HPC |
|GIGA+|Storage Server Storage cluster client|Yes|No|TCP/IP|No|Yes |General HPC |
|ADIOS 2|Storage Server and client|Yes|No|N/A probably IP|No|Yes |General HPC |
|NOVA FS|Storage Server and client|Yes|No|TCP/IP|No|Yes |General HPC |
|Linux CIFS |Local Storage|Yes|No|N/A|No|Yes|Kernel and VM|


[http://www.pdsw.org/pdsw12/slides/grawinkle-Scripted-pNFS-Layouts.pdf](http://www.pdsw.org/pdsw12/slides/grawinkle-Scripted-pNFS-Layouts.pdf)

[http://www.pdsw.org/pdsw14/wips/sasaki-wip-pdsw14.pdf](http://www.pdsw.org/pdsw14/wips/sasaki-wip-pdsw14.pdf)

[http://www.pdsw.org/pdsw14/slides/feng-pdsw14.pdf](http://www.pdsw.org/pdsw14/slides/feng-pdsw14.pdf)

[http://www.pdsw.org/pdsw15/wips/wip-sybrandt-slides.pdf](http://www.pdsw.org/pdsw15/wips/wip-sybrandt-slides.pdf)

[http://www.pdsw.org/pdsw14/slides/zheng-pdsw14.pdf](http://www.pdsw.org/pdsw14/slides/zheng-pdsw14.pdf)

[http://www.pdsw.org/pdsw-discs17/wips/slides/kunkel-wipslides-pdsw-discs17.pdf](http://www.pdsw.org/pdsw-discs17/wips/slides/kunkel-wipslides-pdsw-discs17.pdf)

[http://www.pdsw.org/pdsw-discs17/slides/dorier-pdsw-discs17.pdf](http://www.pdsw.org/pdsw-discs17/slides/dorier-pdsw-discs17.pdf)

[https://www.pdl.cmu.edu/PDL-FTP/FS/IndexFS-SC14.pdf](https://www.pdl.cmu.edu/PDL-FTP/FS/IndexFS-SC14.pdf)

[https://www.pdl.cmu.edu/PDL-FTP/FS/CMU-PDL-14-103.pdf](https://www.pdl.cmu.edu/PDL-FTP/FS/CMU-PDL-14-103.pdf)

[https://www.pdl.cmu.edu/PDL-FTP/FS/FSVA-ACMToS.pdf](https://www.pdl.cmu.edu/PDL-FTP/FS/FSVA-ACMToS.pdf)

[https://github.com/zhengqmark/indexfs](https://github.com/zhengqmark/indexfs)

[https://github.com/vsfs/vsfs](https://github.com/vsfs/vsfs)

[https://github.com/archiecobbs/qos](https://github.com/archiecobbs/qos)

[https://github.com/linuxbox2/linux-pnfs](https://github.com/linuxbox2/linux-pnfs)

[http://www.hpdc.org/](http://www.hpdc.org/)

[https://github.com/viciousviper/CloudFS](https://github.com/viciousviper/CloudFS)

[https://github.com/pcloudcom/pclsync](https://github.com/pcloudcom/pclsync)

[https://github.com/streamich/memfs](https://github.com/streamich/memfs)

[https://github.com/vietor/vxfs](https://github.com/vietor/vxfs)

[https://github.com/cmusatyalab/coda](https://github.com/cmusatyalab/coda)

[https://github.com/varialus/hammer](https://github.com/varialus/hammer)

[https://github.com/intermezzOS](https://github.com/intermezzOS)

[https://github.com/sriramsrao/kfs](https://github.com/sriramsrao/kfs)

[https://github.com/gluster/glusterfs](https://github.com/gluster/glusterfs)

[https://github.com/shuuji3/gfarm_v2/blob/master/OVERVIEW.en](https://github.com/shuuji3/gfarm_v2/blob/master/OVERVIEW.en)





