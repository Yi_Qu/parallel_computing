#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    printf("start");
    int a[8] = {1,2,3,4,5,6,7,8};
    int rec[8] = {0,0,0,0,0,0,0,0};
    int rec2[8] = {0,0,0,0,0,0,0,0};
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    int sendcounts[4] = {2,2,2,2};
    int recconts[4] = {2,2,2,2};
    int senddisplay[4] = {0,2,4,6};
    int recdisplay[4] = {0,2,4,6};
    MPI_Alltoallv(a, sendcounts, senddisplay, MPI_INT, rec, recconts, recdisplay, MPI_INT, MPI_COMM_WORLD);

    printf("Rec for %d \n", rank);
    int i = 0;
    for(i = 0; i < 8; i++)
        printf("%d ", rec[i]);
    printf("________________");
    MPI_Alltoallv(rec, sendcounts, senddisplay, MPI_INT, rec2, recconts, recdisplay, MPI_INT, MPI_COMM_WORLD);
    printf("Rec2 for %d \n", rank);
    i = 0;
    for(i = 0; i < 8; i++)
        printf("%d ", rec2[i]);
    printf("________________");

    MPI_Finalize();
    return 0;
}
