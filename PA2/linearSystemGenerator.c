#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
    int dim;
    if(argc == 1) dim = 100;
    if(argc > 1) dim = atoi(argv[0]);
    int **A = (int**)malloc(dim * sizeof(int*));
    int i = 0, j = 0;
    for(i = 0; i < dim; ++i){
        A[i] = (int*)malloc(dim * sizeof(int));
        for(j = 0; j < dim; ++j)
            A[i][j] = rand() % 1000 + 1;
    }
    int *x = (int*)malloc(dim * sizeof(int));
    for(i = 0; i < dim; ++i)
        x[i] = rand() % 1000 + 1;

    int *d = (int*)malloc(dim * sizeof(int));
    for(i = 0; i < dim; ++i){
        int tmp = 0;
        for (j = 0; j < dim; ++j)
            tmp += A[i][j] * x[j];
        d[i] = tmp;
    }

    printf("\n A:\n");
    for(i = 0; i < dim; ++i){
        for(j = 0; j < dim; ++j)
            printf("%d ", A[i][j]);
        printf("\n");
    }

    printf("\n d:");
    for(i = 0; i < dim; ++i)
        printf("%d ", d[i]);

    printf("\n x:");
    for(i = 0; i < dim; ++i)
        printf("%d ", x[i]);
}


