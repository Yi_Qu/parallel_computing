#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void sequentialSolver(double **A, double *b, double *x1, int dim, int num_iter){
    int l = 0, i = 0, j = 0;
    for(l = 0; l < num_iter; ++l){
        for(i = 0; i < dim; ++i){
            double tmp = 0;
            for(j = 0; j < dim; ++j){
                if(j != i){
                    tmp += A[i][j] * x1[j];
                }
            }
            x1[i] = 1/A[i][i] * (b[i] - tmp);
            //printf("\nx1[%d] = %f ", i, x1[i]);
        }
    }
}

void parallelSolver(double **A, double *b, double *x2, int dim, int num_iter, int ID){
    int num_threads = omp_get_num_threads();
    int istart = dim / num_threads * ID;
    int iend = dim / num_threads * (ID + 1) - 1;
    if(ID == num_threads - 1) iend = dim - 1;
    int l = 0, i = 0, j = 0;
    for(l = 0; l < num_iter; ++l){
        for(i = istart; i <= iend; ++i){
//#pragma omp for
        //for(i = 0; i < dim; ++i){
            double tmp = 0;
            for(j = 0; j < dim; ++j){
                if(j != i){
                    tmp += A[i][j] * x2[j];
                }
            }
            //#pragma omp critical
            x2[i] = 1/A[i][i] * (b[i] - tmp);
            //printf("\nID = %d ****** x2[%d] = %f ", ID, i, x2[i]);
        }
    }
}

int main(int argc, char** argv){
    //Generate A[dim, dim] x[dim, 1] = b[dim, 1]
    //Get max number of threads
    omp_set_dynamic(0);
    int max_threads = 4;
    srand(1);
    int dim;
    if(argc == 1) dim = 10000;
    if(argc > 2){
        dim = atoi(argv[1]);
        max_threads = atoi(argv[2]);
    } else {
        printf("Wrong Input");
        return 0;
    }
    omp_set_num_threads(max_threads);
    
    double **A = (double**)malloc(dim * sizeof(double*));
    int i = 0, j = 0;
    for(i = 0; i < dim; ++i){
        A[i] = (double*)malloc(dim * sizeof(double));
        double tmp = 0;
        for(j = 0; j < dim; ++j){
            A[i][j] = rand() % 100 + 1;
            tmp += A[i][j];
        }
        A[i][i] = tmp;
    }
    double *x = (double*)malloc(dim * sizeof(double));
    for(i = 0; i < dim; ++i)
        x[i] = rand() % 100 + 1;

    double *b = (double*)malloc(dim * sizeof(double));
    for(i = 0; i < dim; ++i){
        double tmp = 0;
        for (j = 0; j < dim; ++j)
            tmp += A[i][j] * x[j];
        b[i] = tmp;
    }

    //Sequential solver
    double *x1 = (double*)malloc(dim * sizeof(double));
    double *x2 = (double*)malloc(dim * sizeof(double));
    memset(x2, 0, dim * sizeof(double));
    memset(x1, 0, dim * sizeof(double));

    int num_iter = 10000;
    double sequential_start = omp_get_wtime();
    sequentialSolver(A, b, x1, dim, num_iter);
    double sequential_end = omp_get_wtime();

    double parallel_start = omp_get_wtime();
#pragma omp parallel
    {
        int ID = omp_get_thread_num();
        parallelSolver(A, b, x2, dim, num_iter, ID);
    }
    double parallel_end = omp_get_wtime();
/*
    printf("\n Parallel CHECK: \n");
    for(i = 0; i < dim; ++i){
        double tmp = 0;
        for(j = 0; j < dim; ++j) tmp += A[i][j] * x2[j];
        printf("%f ", b[i] - tmp);
    }
        //printf("%d ", x1[i] - x[i]);
    printf("\n");

    printf("\n Serial CHECK: \n");
    for(i = 0; i < dim; ++i){
        double tmp = 0;
        for(j = 0; j < dim; ++j) tmp += A[i][j] * x1[j];
        printf("%f ", b[i] - tmp);
    }
        //printf("%d ", x1[i] - x[i]);
    printf("\n");
*/
    printf("Results:\n");
    for(i = 0; i < dim; ++i) printf("%f ", x1[i]);
    printf("\n");

    printf("Cross CHECK: \n");
    for(i = 0; i < dim; ++i){
        if(x2[i] - x1[i] > 0.2) printf("%f ", x2[i] - x1[i]);
    }
    printf("\n");

    printf("Parallel time = %f\n", parallel_end - parallel_start);
    printf("Sequential time = %f\n", sequential_end - sequential_start);

    //free spaces
    free(b);
    free(x1);
    free(x2);
    for(i = 0; i < dim; ++i) free(A[i]);
    free(A);
}
