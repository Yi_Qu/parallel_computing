/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */
/*
 * See COPYRIGHT in top-level directory.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

/*
 * Sort array using blocking send/recv between 2 ranks.
 *
 * The master process prepares the data and sends the latter half
 * of the array to the other rank. Each rank sorts it half. The
 * master then merges the sorted halves together. The two ranks
 * communicate using blocking send/recv.
 */

#define NUM_ELEMENTS 500

static int compare_int(const void *a, const void *b)
{
    return (*(int *) a - *(int *) b);
}

/* Merge sorted arrays a[] and b[] into a[].
 * Length of a[] must be sum of lengths of a[] and b[] */
static void merge(int *a, int numel_a, int *b, int numel_b)
{
    int *sorted = (int *) malloc((numel_a + numel_b) * sizeof *a);
    int i, a_i = 0, b_i = 0;
    /* merge a[] and b[] into sorted[] */
    for (i = 0; i < (numel_a + numel_b); i++) {
        if (a_i < numel_a && b_i < numel_b) {
            if (a[a_i] < b[b_i]) {
                sorted[i] = a[a_i];
                a_i++;
            } else {
                sorted[i] = b[b_i];
                b_i++;
            }
        } else {
            if (a_i < numel_a) {
                sorted[i] = a[a_i];
                a_i++;
            } else {
                sorted[i] = b[b_i];
                b_i++;
            }
        }
    }
    /* copy sorted[] into a[] */
    memcpy(a, sorted, (numel_a + numel_b) * sizeof *sorted);
    free(sorted);
}

int main(int argc, char **argv)
{
    int rank, size, data[NUM_ELEMENTS];
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    /*
    if (size != 2) {
        printf("Please run with exactly 2 ranks\n");
        MPI_Finalize();
        exit(0);
    }
    */
    /*
    int first_half = floor((double) NUM_ELEMENTS / 2);
    int second_half = NUM_ELEMENTS - first_half;
    */
    int bucket_size = floor((double)NUM_ELEMENTS / size);
    int last_size = bucket_size + NUM_ELEMENTS - bucket_size * size;

    srand(0);
    double t1 = MPI_Wtime();
    if (rank == 0) {
        /* prepare data and display it */
        int i;
        printf("Unsorted:\t");
        for (i = 0; i < NUM_ELEMENTS; i++) {
            data[i] = rand() % NUM_ELEMENTS;
            printf("%d ", data[i]);
        }
        printf("\n");

        int prank = 1;
        for (prank = 1; prank < size - 1; prank++){
            /* send prank * bucket_size data to the other rank */
            MPI_Send(&data[prank * bucket_size], bucket_size, MPI_INT, prank, 0, MPI_COMM_WORLD);
            /* sort the first half of the data */
        }
        /* send data to the last process */
        MPI_Send(&data[prank * bucket_size], last_size, MPI_INT, prank, 0, MPI_COMM_WORLD);

        qsort(data, bucket_size, sizeof(int), compare_int);

        prank = 1;
        for (prank = 1; prank < size - 1; prank++){
            /* receive sorted prank * bucket_size of the data */
            MPI_Recv(&data[prank * bucket_size], bucket_size, MPI_INT, prank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            /* merge previous data with current data */
            merge(data, prank * bucket_size, &data[prank * bucket_size], bucket_size);
        }
        /* receive last_size of the data */
        MPI_Recv(&data[prank * bucket_size], last_size, MPI_INT, prank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        /* merge the last part of data (using sort on the whole array) */
        merge(data, prank * bucket_size, &data[prank * bucket_size], last_size);

        /* display sorted array */
        printf("Sorted:\t\t");
        for (i = 0; i < NUM_ELEMENTS; i++)
            printf("%d ", data[i]);
        printf("\n");
    } else if (rank != size - 1) {
        /* receive bucket_size of the data */
        MPI_Recv(data, bucket_size, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        /* sort the received data */
        qsort(data, bucket_size, sizeof(int), compare_int);

        /* send back the sorted data */
        MPI_Send(data, bucket_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    } else { //last process
        MPI_Recv(data, last_size, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        qsort(data, last_size, sizeof(int), compare_int);
        MPI_Send(data, last_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
    double t2 = MPI_Wtime();
    if(rank == 0){
        printf("time = %f\n", t2 - t1);
    }
    MPI_Finalize();
    return 0;
}
